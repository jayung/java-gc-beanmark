package com.jiawaer.javagc.benchmark.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello(@RequestParam(defaultValue = "1", required = false) Integer sleepSeconds) throws InterruptedException {
        log.info("Hello. ");
        this.mockSomeHeavyBiz(sleepSeconds);
        return "Hello. ";
    }

    @GetMapping("/isEnabledVirtualThreadForTomcat")
    public Boolean isEnabledVirtualThreadForTomcat() {
        return false;
    }

    /**
     * 模拟耗时的业务逻辑(IO型)
     *
     * @throws InterruptedException
     */
    public void mockSomeHeavyBiz(Integer sleepSeconds) throws InterruptedException {
        Thread.sleep(sleepSeconds * 1000);
    }

    @GetMapping("/usage")
    public Usage usage() {
        return new Usage(System.getProperty("java.version"),
                Runtime.getRuntime().availableProcessors(),
                Runtime.getRuntime().freeMemory() / 1024 / 1024,
                Runtime.getRuntime().totalMemory() / 1024 / 1024,
                Runtime.getRuntime().maxMemory() / 1024 / 1024);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Usage {
        String jdkVersion;
        int availableProcessors;
        long freeMemory;
        long totalMemory;
        long maxMemory;

        @Override
        public String toString() {
            return jdkVersion +
                    ", " + availableProcessors +
                    ", " + freeMemory +
                    ", " + totalMemory +
                    ", " + maxMemory;
        }
    }

}
