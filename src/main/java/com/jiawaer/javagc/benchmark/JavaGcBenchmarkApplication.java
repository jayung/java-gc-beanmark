package com.jiawaer.javagc.benchmark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaGcBenchmarkApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaGcBenchmarkApplication.class, args);
		System.out.println(System.getProperties().get("java.version"));
		System.getProperties().forEach((k, v) -> System.out.println(k + " = " + v));
	}

}
