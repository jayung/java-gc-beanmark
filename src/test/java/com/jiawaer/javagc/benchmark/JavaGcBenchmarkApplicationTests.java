package com.jiawaer.javagc.benchmark;

import com.jiawaer.javagc.benchmark.controller.HelloController;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

class JavaGcBenchmarkApplicationTests {

    String host = "http://localhost:8081";

    RestTemplate restTemplate = new RestTemplate();

    String isEnabledVirtualThreadForTomcat() {
        String isEnabledVirtualThreadForTomcat = restTemplate.getForObject(host + "/isEnabledVirtualThreadForTomcat", String.class);
        return isEnabledVirtualThreadForTomcat;
    }

    /**
     * 大量并发请求
     */
    void concurrencyRequest(Integer sleepSeconds, Integer concurrencyCount) throws InterruptedException, IOException {
        long startTime = System.currentTimeMillis();
        ExecutorService executorService = Executors.newCachedThreadPool();
        String isEnabledVirtualThreadForTomcat = this.isEnabledVirtualThreadForTomcat();
        System.out.println("是否开启了虚拟线程：" + isEnabledVirtualThreadForTomcat);
        CountDownLatch countDownLatch = new CountDownLatch(concurrencyCount);
        AtomicInteger errorCount = new AtomicInteger();
        for (int i = 1; i <= concurrencyCount; i++) {
            executorService.submit(() -> {
                try {
                    String str = restTemplate.getForObject(host + "/hello?sleepSeconds=" + sleepSeconds, String.class);
                } catch (Exception e) {
                    System.out.println("error: " + e.fillInStackTrace());
                    errorCount.getAndIncrement();
                    e.printStackTrace();
                } finally {
                    countDownLatch.countDown();
                }
            });

        }
        countDownLatch.await();
        double costs = (System.currentTimeMillis() - startTime) / 1000.0; // 秒
        HelloController.Usage result = restTemplate.getForObject(host + "/usage", HelloController.Usage.class);
        String recordFileName = System.getProperty("user.dir") + "\\src\\test\\resources\\gc_record.csv";

        // 往工程目录里的record.csv追加本次测试结果
        Files.write(Paths.get(recordFileName),
                (System.lineSeparator() + result.toString()).getBytes(StandardCharsets.UTF_8),
                StandardOpenOption.APPEND);
    }

    @Test
    void concurrencyRequest() throws IOException, InterruptedException {
        this.concurrencyRequest(2, 4000);
    }


    /**
     * 清空csv数据（保留表头）
     */
    @Test
    void cleanRecordCsv() throws IOException {
        String recordFileName = System.getProperty("user.dir") + "\\src\\test\\resources\\gc_record.csv";
        Files.write(Paths.get(recordFileName),
                "jdk version, free memory, total memory, max memory".getBytes(StandardCharsets.UTF_8),
                StandardOpenOption.TRUNCATE_EXISTING);
    }

    @Test
    void group1() throws IOException, InterruptedException {
        int sleepSeconds1 = 1;
        for (int i = 0; i < 3; i++) {
            this.concurrencyRequest(sleepSeconds1, 1000);
            Thread.sleep(1000);
        }
        for (int i = 0; i < 3; i++) {
            this.concurrencyRequest(sleepSeconds1, 3000);
            Thread.sleep(1000);
        }

    }
}
